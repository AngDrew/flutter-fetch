
import 'geo_model.dart';

class AddressModel{
  String street;
  String suite;
  String city;
  int zipcode;
  GeoModel geo;

  AddressModel(Map<String, dynamic> parsedJson){
    street = parsedJson['street'];
    suite = parsedJson['suite'];
    city = parsedJson['city'];
    zipcode = parsedJson['zipcode'];
  }
}