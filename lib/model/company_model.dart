
class CompanyModel{
  String name;
  String catchPhrase;
  String bs;
  CompanyModel(Map<String, dynamic> parsedJson){
    name = parsedJson['name'];
    catchPhrase = parsedJson['catchPhrase'];
    bs = parsedJson['bs'];
  }
}