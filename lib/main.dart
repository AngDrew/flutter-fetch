import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:retrofit/model/user_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Retrofit Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Retrofit(),
    );
  }
}

class Retrofit extends StatefulWidget {
  @override
  _RetrofitState createState() => _RetrofitState();
}

class _RetrofitState extends State<Retrofit> {
  List<UserModel> users = [];
  int counter = 0;
  @override
  void initState() {
    super.initState();
    fetch();
  }

  void fetch() async {
    Dio dio = new Dio();
    Response response =
        await dio.get('https://jsonplaceholder.typicode.com/users');
    List<dynamic> data = response.data as List<dynamic>;
    setState(() {
      data.forEach((user) => users.add(UserModel(user)));
      counter = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('fetch data'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Column(
              children: List<Widget>.generate(counter, (i) {
                return Text('${users[i].name}');
              }),
            ),
            FlatButton(
              onPressed: () {
                setState(() {
                  counter++;
                });
              },
              child: Text('tambah lagi'),
            ),
          ],
        ),
      ),
    );
  }
}
