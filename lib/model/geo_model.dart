
class GeoModel{
  double lat;
  double lng;

  GeoModel(Map<String, dynamic> parsedJson){
    lat = parsedJson['lat'];
    lng = parsedJson['lng'];
  }
}