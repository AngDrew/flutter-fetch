
import 'package:retrofit/model/address_model.dart';
import 'package:retrofit/model/company_model.dart';

class UserModel{
  int id;
  String name;
  String username;
  String email;
  String phone;
  String website;
  //nanti aja karena agak ribet
  AddressModel address;
  CompanyModel company;

  
  UserModel(Map<String, dynamic> parsedJson){
    id = parsedJson['id'];
    name = parsedJson['name'];
    username = parsedJson['username'];
    email = parsedJson['email'];
  }
}